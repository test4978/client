import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Post, ProfileService } from '../model/profile.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  public updateState: boolean = false;
  public activUpdate: number = 0;

  public postListBehever = new BehaviorSubject<Post[]>([]);
  public postsList$ = this.postListBehever.asObservable();


  public form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    author: new FormControl('', [Validators.required]),
  })

  public formSerch = new FormGroup({
    title: new FormControl('',[Validators.required])
  })

  constructor(
    private cookieService: CookieService,
    private profileService: ProfileService
  ) { }

  
  ngOnInit(): void {
    this.cookieService.set('login', 'true');
    this.profileService.find().subscribe(p => this.postListBehever.next(p));
  }
  
  hendelSubmit() {
    if (this.updateState) {
      this.profileService.update({...this.form.value, id: this.activUpdate}).subscribe(null,null,() => {
        this.profileService.find().subscribe(p => this.postListBehever.next(p));
      });
    } else {
      this.profileService.create(this.form.value).subscribe();
      this.postListBehever.next([...this.postListBehever.getValue(), this.form.value]);
    }
  }

  hendelDelete(id: number) {
    this.profileService.delete(id).subscribe();
    this.postListBehever.next([...this.postListBehever.getValue().filter(a => a.id != id)])
  }

  hendelUpdate(id: number) {
    let postsList:Post[] = [];
    this.postsList$.subscribe(p => postsList = p);

    this.form.patchValue({
      title: postsList[id-1].title,
      author:postsList[id-1].author
    });

    this.updateState = true;
    this.activUpdate = id;
  }

  // #  Методы примитивного поиска и сброса фильтров 
  hendelSerch() {
    this.postListBehever.next(this.postListBehever.getValue().filter(post => post.title == this.formSerch.value.title))
  }

  hendelCancel() {
    this.profileService.find().subscribe(p => this.postListBehever.next(p));
  }
}
