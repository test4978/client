import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


const AUTH_HOST = `${environment.apiHost}/profile`;

type loginData = {
  password:string
  login: string
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  private isAuthSubject = new BehaviorSubject<boolean>(false);
  public isAuth$ = this.isAuthSubject.asObservable();
  
  constructor(
    private http: HttpClient,
  ) { }


  public loginCheck(data: loginData): Observable<any> {
    return this.http.get<any>(AUTH_HOST).pipe(
      tap(({login, password}) => this.isAuthSubject.next((login == data.login && password == data.password )))
    );
  }
}
