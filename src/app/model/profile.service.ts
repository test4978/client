import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const POST_HOST = `${environment.apiHost}/posts`;


export type Post = {
  id: number,
  title: string,
  author: string
}

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http: HttpClient,
  ) { }

  public find(): Observable<Post[]> {
    return this.http.get<Post[]>(POST_HOST);
  }

  public create(data: Partial<Post>): Observable<Post> {
    return this.http.post<Post>(POST_HOST, data);
  }

  public update(data: Post): Observable<void> {
    return this.http.put<void>(`${POST_HOST}/${data.id}`, data);
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(`${POST_HOST}/${id}`);
  }
}
