import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { LoginService } from '../model/login-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(
    private loginService: LoginService,
    private router: Router,
    private cookieService: CookieService
  ){ };
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.cookieService.get('login') == "true") {
      return true
    } else {
      return this.loginService.isAuth$.pipe(
        tap(e => {
          if (!e) {
            this.router.navigateByUrl('')
          }
        })
      );
    }
  }
  
}
